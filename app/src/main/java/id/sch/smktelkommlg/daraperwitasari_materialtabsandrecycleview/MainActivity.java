package id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Assets.TabLayoutAdapter;
import id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Item.TabItem_RecylerView_Horizontal;
import id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Item.TabItem_RecylerView_Vertical;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabLayoutAdapter tabLayoutAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.main_tablayout);
        viewPager = findViewById(R.id.main_viewpager);

        //Initial TabLayout Adapter
        tabLayoutAdapter = new TabLayoutAdapter(getSupportFragmentManager());

        tabLayoutAdapter.AddFragmentPage(new TabItem_RecylerView_Vertical(), "Vertical");
        tabLayoutAdapter.AddFragmentPage(new TabItem_RecylerView_Horizontal(), "Horizontal");

        viewPager.setAdapter(tabLayoutAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}

