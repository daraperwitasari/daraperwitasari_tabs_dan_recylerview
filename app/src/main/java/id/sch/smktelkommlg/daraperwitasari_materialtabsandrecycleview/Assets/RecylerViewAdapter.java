package id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Assets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.R;

public class RecylerViewAdapter extends RecyclerView.Adapter<RecylerViewAdapter.RecylerViewHolder> {

    public Context context;
    public ArrayList<ItemList> itemLists;
    public String Code;

    public RecylerViewAdapter(Context context, ArrayList<ItemList> berandaLists, String code) {
        this.context = context;
        this.itemLists = berandaLists;
        this.Code = code;
    }

    @NonNull
    @Override
    public RecylerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int id;

        if (Code.equals("vertical")) {
            id = R.layout.recyleritem_vertical;
        } else {
            id = R.layout.recyleritem_horizontal;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(id, parent, false);
        return new RecylerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecylerViewHolder holder, int position) {
        ItemList mitemLists = itemLists.get(position);

        holder.Logo.setBackground(mitemLists.Gambar);
        holder.Tittle.setText(mitemLists.Text);
    }

    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    class RecylerViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout Logo;
        private TextView Tittle;

        public RecylerViewHolder(View itemView) {
            super(itemView);

            Logo = itemView.findViewById(R.id.Logo_Item);
            Tittle = itemView.findViewById(R.id.Tittle_Item);
        }
    }
}
