package id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Assets;

import android.graphics.drawable.Drawable;

public class ItemList {
    public Drawable Gambar;
    public String Text;

    public ItemList(Drawable gambar, String text) {
        Gambar = gambar;
        Text = text;
    }
}