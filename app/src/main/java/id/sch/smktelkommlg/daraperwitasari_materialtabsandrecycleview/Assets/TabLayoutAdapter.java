package id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Assets;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabLayoutAdapter extends FragmentStatePagerAdapter {

    public String myTittle = null;
    private List<Fragment> mFragment = new ArrayList<>();
    private List<String> mTittle = new ArrayList<>();

    public TabLayoutAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void AddFragmentPage(Fragment fragment, String tittle) {
        this.mFragment.add(fragment);
        this.mTittle.add(tittle);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTittle.get(position);
    }

    @Override
    public int getCount() {
        return mTittle.size();
    }
}
