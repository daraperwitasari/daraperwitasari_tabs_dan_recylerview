package id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Item;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Assets.ItemList;
import id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.Assets.RecylerViewAdapter;
import id.sch.smktelkommlg.daraperwitasari_materialtabsandrecycleview.R;

public class TabItem_RecylerView_Horizontal extends Fragment {
    public View view;
    private RecyclerView mrecyclerView;

    private ArrayList<ItemList> mitemList = new ArrayList<>();
    private RecylerViewAdapter mrecylerViewAdapter;

    public TabItem_RecylerView_Horizontal() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tabitem_recylerview_horizontal, container, false);

        mrecyclerView = view.findViewById(R.id.IDtbitem_recyler_horizontal);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mrecyclerView.setLayoutManager(linearLayoutManager);
        mrecylerViewAdapter = new RecylerViewAdapter(getContext(), mitemList, "horizontal");
        mrecyclerView.setAdapter(mrecylerViewAdapter);

        DoBindData();

        return view;
    }

    public void DoBindData() {
        Resources resources = getResources();
        String[] Tittle = resources.getStringArray(R.array.tittle);
        TypedArray a = resources.obtainTypedArray(R.array.gambar);
        Drawable[] Gambar = new Drawable[a.length()];

        for (int i = 0; i < Gambar.length; i++) {
            Gambar[i] = a.getDrawable(i);
        }
        for (int i = 0; i < Tittle.length; i++) {
            mitemList.add(new ItemList(Gambar[i], Tittle[i]));
        }

        mrecylerViewAdapter.notifyDataSetChanged();
    }
}
